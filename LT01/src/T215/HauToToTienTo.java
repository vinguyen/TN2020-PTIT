package T215;

import java.util.Scanner;
import java.util.Stack;

public class HauToToTienTo {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        String chuoi = "+-/*^%";
        in.nextLine();
        while (test-- > 0) {
            String str = in.nextLine();
            String[] arrStr = str.split("");
            Stack<String> stack = new Stack<>();
            for (String s : arrStr) {
                if (chuoi.contains(s)) {
                    String a = stack.pop();
                    String b = stack.pop();
                    stack.add(s + b + a);
                } else stack.add(s);
            }
            System.out.println(stack.pop());
        }
    }

}
