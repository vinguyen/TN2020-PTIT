package T309;

import java.util.Scanner;


public class main {
     public static long mod=1000000007;
     public static long[] f;
    public static long ktra(int n,int k){
        f=new long[n+1];
        f[0]=1;
        f[1]=1;
        for(int i=2 ; i<n ; i++){
            f[i]=0;
            for(int j=1; j<=k && j<=i; j++){
                f[i]=(f[i]+f[i-j])%mod;
                
            }
        }
        return f[n-1];
    }
    
    public static long bacThang(int s,int m){
        return ktra(s+1,m);
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int test= sc. nextInt();
        while(test-->0){
            int n=sc.nextInt();
            int k=sc.nextInt();
             System.out.println(bacThang(n,k));
        }
    }
    
}