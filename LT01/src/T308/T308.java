package T308;

import java.util.Scanner;

// To hop
public class T308 {

    static long mod = 1000000007;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int n = in.nextInt();
            int k = in.nextInt();
            System.out.println(toHop(n,k));
        }
    }

    public static long toHop(int n, int k) {
        long[][] F = new long[n + 1][n + 1];
        for (int i = 1; i <= n; i++) {
            for (int j = 0; j <= i; j++) {
                if (j == 0 || i == j) {
                    F[i][j] = 1;
                }
                else {
                    F[i][j] = (F[i-1][j-1]+F[i-1][j])%mod;
                }
            }
        }

        return F[n][k];
    }

}
