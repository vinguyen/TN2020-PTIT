package T308;

import java.util.Scanner;

public class T308_ {

    static long mod = 1000000007;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int n = in.nextInt();
            int k = in.nextInt();
            long[][] F = new long[n + 5][n + 5];

            for (int i = 1; i <= n; i++) {
                for (int j = 0; j <= i; j++) {
                    if (j == 0 || i == j) {
                        F[i][j] = 1;
                    } else {
                        F[i][j] = (F[i-1][j-1]+F[i-1][j])%mod;
                    }
                }
            }

            System.out.println(F[n][k]);

        }
    }

}
