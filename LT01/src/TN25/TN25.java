package TN25;

import java.util.Scanner;

public class TN25 {

    static int n;
    static int[] X = new int[11];
    static int[] check = new int[20];
    static String str;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            str = in.nextLine();
            n = str.length();
            Try(1);
            System.out.println();
        }
    }

    public static void Try(int i) {
        for (int j = 1; j <= n; j++) {
            if (check[j] == 0) {
                X[i] = j;
                check[j] = 1;
                if (i == n) {
                    for (int k = 1; k <= n; k++) {
                        System.out.print(str.charAt(X[k]-1));
                    }
                    System.out.print(" ");
                }
                else Try(i+1);
                check[j] = 0;
            }
        }
    }

}
