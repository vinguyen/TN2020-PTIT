package T313;

import java.util.Scanner;

public class ConBo {

    static int C;
    static int n;
    static int[] W;
    static int[] F;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        C = in.nextInt();
        n = in.nextInt();
        W = new int[n + 2];
        F = new int[n + 2];
        for (int i = 0; i < n; i++) {
            W[i] = in.nextInt();
            F[i] = 0;
        }
        int result = -1;

        for (int i = n - 1; i >= 0; i--) {
            if (F[i] == 0) {
                F[i]=1;
                for (int j = i + 1; j < n; j++) {
                    F[j]=0;
                }
                if (sum() > result && sum() < C) {
                    result = sum();
                }
                i=n;
            }
        }
        System.out.println(result);

    }

    public static int sum() {
        int sum = 0;

        for (int i = 0; i < n; i++) {
            if (F[i] == 1) {
                sum += W[i];
            }
        }
        return sum;
    }

}
