package T329;

import java.util.Scanner;

public class T329_ {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int m = in.nextInt();
        long[][] kq = new long[n + 5][n + 5];

        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= n; j++) {
                kq[i][j] = 1000000L;
            }
        }

        for (int i = 1; i <= m; i++) {
            int u = in.nextInt();
            int v = in.nextInt();
            int c = in.nextInt();
            kq[u][v] = kq[v][u] = c;
        }

        for (int k = 1; k <= n; k++) {
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= n; j++) {
                    kq[i][j] = Math.min(kq[i][j], kq[i][k] + kq[k][j]);
                }
            }
        }

        int q = in.nextInt();
        while (q-- > 0) {
            int u = in.nextInt();
            int v = in.nextInt();
            if(u!=v) System.out.println(kq[u][v]);
            else System.out.println(0);
        }

    }

}
