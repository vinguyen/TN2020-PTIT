package TN28;

import java.util.Arrays;
import java.util.Scanner;

public class TN28 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            StringBuilder str1 = new StringBuilder();
            StringBuilder str2 = new StringBuilder();
            int n = in.nextInt();
            int[] arr = new int[n];
            for (int i = 0; i < n; i++) {
                arr[i] = in.nextInt();
            }
            Arrays.sort(arr);
            for (int i = 0; i < n; i++) {
                if (i % 2 == 0) {
                    str1.append(arr[i]);
                } else str2.append(arr[i]);
            }
            System.out.println(Long.parseLong(str1.toString()) + Long.parseLong(str2.toString()));
        }
    }

}
