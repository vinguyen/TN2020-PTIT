package T213;

import java.util.Scanner;
import java.util.Stack;

public class main {

    public static Scanner sc;
    public static String s;

    public static void ktra() {
        int dem = 0;
        Stack<Character> st = new Stack<Character>();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                st.push('(');
            } else {
                if (st.empty()) {
                    dem++;
                    st.push('(');
                } else {
                    st.pop();
                }

            }
        }
        dem += st.size() / 2;
        System.out.println(dem);
    }

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int test = sc.nextInt();
        s = sc.nextLine();
        while (test-- > 0) {
            s = sc.nextLine();
            ktra();
        }
    }
}