package T210;

import java.util.Scanner;
import java.util.Stack;

public class main {

    public static Scanner sc;
    public static int n;
    public static long mod = 1000000007;
    public static String s;

    public static void ktra() {
        Stack<Character> st = new Stack<Character>();

        for (int i = 0; i < s.length(); i++) {
            char x = s.charAt(i);
            if (x == '[' || x == '(' || x == '{') {
                st.push(x);
            } else {
                if (st.empty()) {
                    System.out.println("NO");
                    return;
                }
                char y = st.pop();
                if (x == '(' && y != ')') {
                    System.out.println("NO");
                    return;
                }
                if (x == '[' && y != ']') {
                    System.out.println("NO");
                    return;
                }
                if (x == '{' && y != '}') {
                    System.out.println("NO");
                    return;
                }

            }
        }
        if (!st.empty()) {
            System.out.println("NO");
            return;
        }
        System.out.println("YES");
    }

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int test = Integer.parseInt(sc.nextLine());
        while (test-- > 0) {
            s = sc.nextLine();
            ktra();
        }
    }
}