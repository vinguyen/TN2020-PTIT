package T210;

import java.util.Scanner;
import java.util.Stack;

public class DayNgoacDung {

    static String str;


    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int test = in.nextInt();
        in.nextLine();

        while (test-- > 0) {
            str = in.nextLine();
            Try();
        }
    }

    public static void Try() {

        if (str.length() == 0) {
            System.out.println("YES");
            return;
        }

        Stack<Character> stack = new Stack<Character>();

        for (int i = 0; i < str.length(); i++) {
            char x = str.charAt(i);
            if (x == '[' || x == '(' || x == '{') {
                stack.push(x);
            } else if (stack.empty() && (x == ']' || x == ')' || x == '}')) {
                System.out.println("NO");
                return;
            }
            else {
                char y = stack.pop();
                if ((y == '{' && x != '}') || (y == '(' && x != ')') || (y == '[' && x != ']') ) {
                    System.out.println("NO");
                    return;
                }
            }
        }
        if (!stack.empty()) {
            System.out.println("NO");
            return;
        }
        System.out.println("YES");

    }

}
