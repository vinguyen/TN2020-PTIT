package T217;

import java.util.Scanner;
import java.util.Stack;

public class main{

    public static Scanner sc;
    public static String s;

    public static void ktra() {
        Stack<Integer> st = new Stack<Integer>();
        for (int i = s.length() -1 ; i >= 0 ; i--) {
            char x = s.charAt(i);
            if (x == '+') {
                int a = st.pop();
                int b = st.pop();
                st.add(a+b);
            } else if (x == '-') {
                int a = st.pop();
                int b = st.pop();
                st.add(a-b);
            } else if (x == '*') {
                int a = st.pop();
                int b = st.pop();
                st.add(a*b);
            } else if (x == '/') {
                int a = st.pop();
                int b = st.pop();
                st.add(a/b);
            } else {
                int t = x - '0';
                st.add(t);
            }
        }
        System.out.println(st.pop());
    }

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int test = sc.nextInt();
        s = sc.nextLine();
        while (test-- > 0) {
            s = sc.nextLine();
            ktra();
        }
    }
}