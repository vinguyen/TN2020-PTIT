package T209;

import java.util.Scanner;

public class DaoTu {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String str = in.nextLine();
            Try(str);
        }

    }

    public static void Try(String str) {

        String[] arrStr = str.split(" ");
        int length = arrStr.length;

        for (int i = 0; i < length - 1; i++) {
            System.out.print((new StringBuffer(arrStr[i])).reverse().toString()+" ");
        }
        System.out.println((new StringBuffer(arrStr[length-1])).reverse().toString());
    }

}
