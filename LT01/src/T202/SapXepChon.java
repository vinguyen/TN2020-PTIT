package T202;

import java.util.Scanner;

public class SapXepChon {

    public static int n;
    public static int[] arr;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        n = in.nextInt();
        arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = in.nextInt();
        }

        int min_idx;
        int temp;

        for (int i = 0; i < n - 1; i++) {
            min_idx = i;
            for (int j = i + 1; j < n; j++) {
                if (arr[min_idx] > arr[j]) {
                    min_idx = j;
                }
            }
            temp = arr[i];
            arr[i] = arr[min_idx];
            arr[min_idx] = temp;
            view(i);
        }
    }

    public static void view(int i) {
        System.out.print("Buoc "+(i+1)+": ");
        for (int k = 0; k < n-1; k++) {
            System.out.print(arr[k]+" ");
        }
        System.out.print(arr[n-1]);
        System.out.println();
    }

}
