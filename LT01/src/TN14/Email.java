package TN14;

import java.util.*;

public class Email {
    static int n;
    static long base = (long)(1e9+7);
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int t = input.nextInt();
        while(t-->0){
            n = input.nextInt();
            PriorityQueue<Long> q = new PriorityQueue();
            for(int i = 1; i <= n; i++){
                int x = input.nextInt();
                q.add((long)x);
            }
            long ans = 0;
            while(q.size()>1){
                long a = q.poll();
                long b = q.poll();
                ans = (ans+(a+b)%base)%base;
                q.add((a+b)%base);
            }
            System.out.println(ans);
        }
    }
}