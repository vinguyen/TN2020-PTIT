package TN14;

import java.util.PriorityQueue;
import java.util.Scanner;

public class TN14 {
    public static long mod = 1000000007;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();

        while (test-- > 0) {
            int n = in.nextInt();
            PriorityQueue<Long> queue = new PriorityQueue<>();
            for (int i = 0; i < n; i++) {
                queue.add(in.nextLong());
            }
            long kq = 0;
            while (queue.size() > 1) {
                long a = queue.poll();
                long b = queue.poll();
                kq = (kq+(a+b)%mod)%mod;
                queue.add((a+b)%mod);
            }
            System.out.println(kq);
        }

    }
}
