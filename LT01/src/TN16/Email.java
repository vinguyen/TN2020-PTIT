package TN16;

import java.util.*;

public class Email {
    static int n;
    static int[][] a = new int[200][200];
    static boolean has_road;
    static ArrayList<String> ans;

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int t = input.nextInt();
        while(t-->0){
            n = input.nextInt();
            for(int i = 1; i <= n; i++){
                for(int j = 1; j <= n; j++){
                    a[i][j] = input.nextInt();
                }
            }
            if(a[1][1]==0){
                System.out.println(-1);
                continue;
            }else{
                a[1][1] = 0;
            }
            has_road = false;
            ans = new ArrayList<>();
            dq(1, 1, new StringBuffer(""));
            if(!has_road){
                System.out.println(-1);
            }else{
                Collections.sort(ans);
                for(String x: ans) System.out.print(x + " ");
                System.out.println();
            }
        }
    }

    static void dq(int r, int c, StringBuffer s){
        if(r==n&&c==n){
            ans.add(new String(s));
            has_road = true;
            return;
        }
        if(r<n && a[r+1][c]==1){
            a[r+1][c]=0;
            dq(r+1, c, s.append('D'));
            s.deleteCharAt(s.length()-1);
            a[r+1][c]=1;
        }
        if(c>1 && a[r][c-1]==1){
            a[r][c-1]=0;
            dq(r, c-1, s.append('L'));
            s.deleteCharAt(s.length()-1);
            a[r][c-1]=1;
        }
        if(c<n && a[r][c+1]==1){
            a[r][c+1]=0;
            dq(r, c+1, s.append('R'));
            s.deleteCharAt(s.length()-1);
            a[r][c+1]=1;
        }
        if(r>1 && a[r-1][c]==1){
            a[r-1][c]=0;
            dq(r-1, c, s.append('U'));
            s.deleteCharAt(s.length()-1);
            a[r-1][c]=1;
        }
    }
}