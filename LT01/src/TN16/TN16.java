package TN16;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class TN16 {
    static int n;
    static int[][] arr = new int[20][20];
    static boolean has_road;
    static ArrayList<String> kq;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            n = in.nextInt();
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= n; j++) {
                    arr[i][j] = in.nextInt();
                }
            }
            if (arr[1][1] == 0) {
                System.out.println(-1);
                continue;
            }
            else {
                arr[1][1] = 0;
            }
            has_road = false;
            kq = new ArrayList<>();
            timDuong(1, 1, new StringBuffer(""));
            if(has_road) {
                Collections.sort(kq);
                for (String s : kq) {
                    System.out.print(s + " ");
                }
                System.out.println();
            }
            else System.out.println("-1");
        }
    }

    public static void timDuong(int i, int j, StringBuffer strB) {
        if (i == n && j == n) {
            kq.add(new String(strB));
            has_road = true;
            return;
        }
        if (i < n && arr[i + 1][j] == 1) {
            arr[i+1][j]=0;
            timDuong(i + 1, j, strB.append("D"));
            strB.deleteCharAt(strB.length() - 1);
            arr[i+1][j]=1;
        }
        if (j > 1 && arr[i][j - 1] == 1) {
            arr[i][j - 1] = 0;
            timDuong(i, j - 1, strB.append("L"));
            strB.deleteCharAt(strB.length() - 1);
            arr[i][j - 1] = 1;
        }
        if (j < n && arr[i][j + 1] == 1) {
            arr[i][j + 1] = 0;
            timDuong(i,j+1,strB.append("R"));
            strB.deleteCharAt(strB.length() - 1);
            arr[i][j + 1] = 1;
        }
        if (i > 1 && arr[i - 1][j] == 1) {
            arr[i - 1][j] = 0;
            timDuong(i - 1, j, strB.append("U"));
            strB.deleteCharAt(strB.length() - 1);
            arr[i - 1][j] = 1;
        }
    }


}
