package T321;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

// DFS Vo huong
public class T321 {

    static ArrayList<ArrayList<Integer>> lists;
    static boolean[] check;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int v = in.nextInt();
            int e = in.nextInt();
            int u = in.nextInt();
            lists = new ArrayList<>();
            for (int i = 0; i <= v + 5; i++) {
                lists.add(new ArrayList<>());
            }
            for (int i = 1; i <= e; i++) {
                int a = in.nextInt();
                int b = in.nextInt();
                lists.get(a).add(b);
                lists.get(b).add(a);
            }
            check = new boolean[v + 5];
            Arrays.fill(check, true);
            DFS(u);
            System.out.println();
        }
    }

    public static void DFS(int u) {
        check[u] = false;
        System.out.print(u + " ");
        for (int i = 0; i < lists.get(u).size(); i++) {
            int v = lists.get(u).get(i);
            if (check[v]) {
                DFS(v);
            }
        }
    }

}
