package T304;

import java.util.Scanner;

public class T304_ {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] A = new int[n + 5];
        for (int i = 1; i <= n; i++) {
            A[i] = in.nextInt();
        }
        int[] F = new int[n + 5];

        int result = 0;
        for (int i = 1; i <= n; i++) {
            F[i]=1;
            for (int j = 1; j < i; j++) {
                if (A[i] > A[j]) {
                    F[i] = Math.max(F[i], F[j] + 1);
                }
            }
            result = Math.max(result, F[i]);
        }
        System.out.println(result);

    }

}
