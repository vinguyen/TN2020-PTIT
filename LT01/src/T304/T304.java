package T304;

import java.util.Scanner;

// Day con tang dai nhat
public class T304 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] A = new int[n + 5];
        for (int i = 1; i <= n; i++) {
            A[i] = in.nextInt();
        }

        int[] F = new int[n + 5];
        int result = 0;
        for (int i = 1; i <= n; i++) {
            F[i] = 1;
            for (int j = 0; j < i; j++) {
                if (A[i] > A[j]) {
                    F[i] = Math.max(F[i], F[j] + 1);
                }
            }
            result = Math.max(F[i], result);
        }
        System.out.println(result);
    }

}
