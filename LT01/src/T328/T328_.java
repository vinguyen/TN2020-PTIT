package T328;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Scanner;

public class T328_ {

    static int v,e,u;
    static boolean[] check = new boolean[1005];
    static long[] before = new long[1005];
    static ArrayList<ArrayList<Integer>> lists;

    static int base = 100000;
    static PriorityQueue<Long> queue = new PriorityQueue<>();

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            v = in.nextInt();
            e = in.nextInt();
            u = in.nextInt();
            lists = new ArrayList<>();

            for (int i = 0; i <= v; i++) {
                lists.add(new ArrayList<>());
                before[i] = 100000L;
            }

            for (int i = 1; i <= e; i++) {
                int a = in.nextInt();
                int b = in.nextInt();
                int c = in.nextInt();
                lists.get(a).add(c * base + b);
                lists.get(b).add(c * base + a);
            }

            Arrays.fill(check, true);

            check[u] = false;
            before[u] = 0;

            for (long t : lists.get(u)) {
                queue.add(t);
                before[(int) t % base] = t / base;
            }

            while (!queue.isEmpty()) {
                long temp = queue.poll();
                int i = (int) (temp % base);
                long c = temp / base;
                if (c != before[i] || !check[i]) {
                    continue;
                }
                check[i] = false;
                for (long x : lists.get(i)) {
                    int t = (int) (x % base);
                    if (!check[t]) {
                        continue;
                    }
                    long k = x / base;
                    if (before[t] > k + c) {
                        before[t] = k + c;
                        queue.add(base * before[t] + t);
                    }
                }
            }

            for (int i = 1; i <= v; i++) {
                if (i != u) {
                    System.out.print(before[i]+" ");
                }
                else System.out.print("0 ");
            }
            System.out.println();

        }
    }

}
