package T328;// package tt;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

public class t {
	static int t, n, m, o;
	static long[] len = new long[100005];
	static long base = 100000L;
	static boolean[] used = new boolean[100005];
	static PriorityQueue<Long> q = new PriorityQueue<Long>();
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		t = input.nextInt();
		while(t-->0) {
			n = input.nextInt();
			m = input.nextInt();
			o = input.nextInt();
			ArrayList<ArrayList<Long>> adj = new ArrayList();
			for(int i = 0; i <= n; i++) {
				adj.add(new ArrayList<Long>());
				used[i] = false;
				len[i] = 100000000000000000L;
			}
			for(int i = 1; i <= m; i++) {
				int u = input.nextInt();
				int v = input.nextInt();
				int c = input.nextInt();
				adj.get(u).add(1L*(c*base+v));
				adj.get(v).add(1L*(c*base+u));
			}
			used[o] = true;
			len[o] = 0;
			for(long x: adj.get(o)) {
				q.add(x);
				len[(int)(x%base)] = x/base;
			}
			while(!q.isEmpty()) {
				long tmp = q.poll();
				int ver = (int)(tmp%base);
				long c = tmp/base;
				if(c!=len[ver] || used[ver]) continue;	
				used[ver] = true;
				for(long x: adj.get(ver)) {
					int nx_ver = (int)(x%base);
					if(used[nx_ver]) continue;
					long nx_c = x/base;
					if(len[nx_ver] > nx_c+c) {
						len[nx_ver] = nx_c+c;
						q.add(1l*(base*len[nx_ver]+nx_ver));
					}
				}
			}
			for(int i = 1; i <= n; i++) {
				if(i!=o) System.out.print(len[i] + " ");
				else System.out.print("0 ");
			}
			System.out.println();
		}
		
	}
}