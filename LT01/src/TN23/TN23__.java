package TN23;

import java.util.Scanner;

public class TN23__ {

    static int n,s,dem;
    static int[] arr = new int[31];

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            n = in.nextInt();
            s = in.nextInt();
            for (int i = 1; i <= n; i++) {
                arr[i] = in.nextInt();
            }
            dem = 31;
            Try(1, 0, 0);
            if (dem == 31) {
                System.out.println(-1);
            }
            else System.out.println(dem);
        }
    }

    public static void Try(int i, int sum, int c) {
        if(i>n) return;
        Try(i + 1, sum, c);
        if (arr[i] + sum < s) {
            if (c + 1 < dem) {
                Try(i+1,arr[i]+sum,c+1);
            }
        }
        else {
            if (arr[i] + sum == s) {
                dem = Math.min(dem, c + 1);
            }
        }

    }

}
