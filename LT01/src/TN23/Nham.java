package TN23;

import java.util.Scanner;
public class Nham {
    static int t, s, n, ans;
    static int[] a = new int[35];
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        t = input.nextInt();
        while(t-->0){
            n = input.nextInt();
            s = input.nextInt();
            for(int i = 1; i <= n; i++) a[i] = input.nextInt();
            ans = 35;
            dq(1, 0, 0);
            if(ans==35){
                System.out.println(-1);
            }else{
                System.out.println(ans);
            }
        }
    }
    static void dq(int idx, int tmps, int c){
        if(idx>n) return;
        dq(idx+1, tmps, c);
        if(tmps+a[idx]<s){
             if(c+1<ans) dq(idx+1, tmps+a[idx], c+1);
        }else{
            if(tmps+a[idx]==s){
                ans = Math.min(ans, c+1);
            }
        }
    }
}