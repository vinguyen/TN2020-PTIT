package TN08;

import java.util.Scanner;

public class TN08__ {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String str = in.nextLine();
            Try(str);
        }
    }

    public static void Try(String str) {
        if (!str.contains("0")) {
            System.out.println(str.replaceAll("1", "0"));
        }
        else {
            int i = str.length() - 1;
            String[] arrStr = str.split("");

            while (i > -1 && arrStr[i].contains("1")) {
                arrStr[i] = "0";
                i--;
            }
            if (i > 0) {
                arrStr[i] = "1";
            }
            StringBuilder kq = new StringBuilder();
            for (String s : arrStr) {
                kq.append(s);
            }
            System.out.println(kq.toString());
        }
    }

}
