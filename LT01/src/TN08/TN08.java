package TN08;

import java.util.Scanner;

public class TN08 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String str = in.nextLine();
            System.out.println(nextBitString(str));;
        }
    }

    public static String nextBitString(String str) {
        if (!str.contains("0")) {
            return str.replaceAll("1", "0");
        }
        else {
            String[] arrStr = str.split("");
            int i = str.length()-1;

            while (i > 0 && arrStr[i].contains("1")) {
                arrStr[i]="0";
                i--;
            }
            if(i>0) arrStr[i] = "1";

            StringBuilder kq = new StringBuilder();
            for (String s : arrStr) {
                kq.append(s);
            }

            return kq.toString();

        }
    }

}
