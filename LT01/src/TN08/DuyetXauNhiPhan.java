package TN08;

import java.util.Scanner;

public class DuyetXauNhiPhan {

    public static int n;
    public static int[] X = new int[20];
    public static int[] check = new int[20];

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            n = in.nextInt();
            TryNext(1);
            System.out.println();
            TryReverse(1);
            System.out.println();
        }
    }

    public static void TryNext(int i) {
        for (int j = 0; j <= 1; j++) {
            X[i] = j;
            if (i == n) {
                for (int k = 1; k <= n; k++) {
                    System.out.print(X[k]);
                }
                System.out.print(" ");
            } else TryNext(i + 1);
        }
    }

    public static void TryReverse(int i) {
        for (int j = 1; j >= 0; j--) {
            X[i] = j;
            if (i == n) {
                for (int k = 1; k <= n; k++) {
                    System.out.print(X[k]);
                }
                System.out.print(" ");
            }
            else TryReverse(i+1);
        }
    }

}
