package TN08;

import java.util.Scanner;

public class TN08_ {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String str = in.nextLine();
            System.out.println(Try(str));
        }
    }

    public static String Try(String str) {
        if (!str.contains("0")) {
            return str.replaceAll("1", "0");
        }
        else {
            int i = str.length() - 1;
            String[] arrStr = str.split("");

            StringBuilder kq = new StringBuilder();

            while (i > -1 && arrStr[i].contains("1")) {
                arrStr[i] = "0";
                i--;
            }
            if (i > 0) {
                arrStr[i] = "1";
            }

            for (String s : arrStr) {
                kq.append(s);
            }
            return kq.toString();
        }
    }

}
