package T211;

import java.util.Scanner;
import java.util.Stack;

public class main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Scanner sc1 = new Scanner(System.in);
		int test = sc1.nextInt();
			while (test-- > 0) {
			int len=0;
			String s = sc.nextLine();
			int [] a = new int[s.length()];
			Stack<Character> st = new Stack<Character>();
			Stack<Integer> index = new Stack<Integer>();
			for(int i=0;i<s.length();i++) {
				if(s.charAt(i)=='(') {
					st.push('(');
					index.push(i);
				}else {
					if(!st.isEmpty()) {
						char topChar  = st.peek();
						int topIndex = index.peek();
						if(topChar == '(') {
							a[topIndex]=1;
							a[i] =1;
							st.pop();
							index.pop();
							
						}
					}
				}
			}
// 			for(int i=0;i<s.length();i++) {
// 				System.out.print(a[i] + " ");
// 			}
// 			System.out.println();
			for(int i=0; i<s.length();i++) {
				if (a[i]==1) {
					for(int j=i;j<s.length();j++) {
						if(a[j]==0 || j == s.length()-1) {
							if(a[j]==0) len = Math.max(len, j-i);
							else len = Math.max(len, j-i+1);
							i=j;
							break;
						}
					}
				}
				
			}
			System.out.println(len);
		}
	}
}