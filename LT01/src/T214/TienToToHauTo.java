package T214;

import java.util.Scanner;
import java.util.Stack;

public class TienToToHauTo {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        String chuoi = "+-*/^%";
        while (test-- > 0) {
            String str = in.nextLine();
            String[] arrStr = str.split("");
            Stack<String> stack = new Stack<>();
            for (int i = arrStr.length - 1; i >= 0; i--) {
                if (chuoi.contains(arrStr[i])) {
                    String a = stack.pop();
                    String b = stack.pop();
                    stack.add(a + b + arrStr[i]);
                } else stack.add(arrStr[i]);
            }
            System.out.println(stack.pop());
        }

    }

}
