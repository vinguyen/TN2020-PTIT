package TN11;

import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class TN11_ {

    static int n;
    static String str;
    static Set<String> arrStr;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            n = in.nextInt();
            in.nextLine();
            str = in.nextLine();
            arrStr = new TreeSet<>();
            Try(0, new StringBuilder(""));
            for (String s : arrStr) {
                System.out.print(s+" ");
            }
            System.out.println();
        }
    }

    public static void Try(int i, StringBuilder strB) {
        if (i == n) {
            if (strB.length() > 0) {
                arrStr.add(new String(strB));
            }
            return;
        }
        Try(i + 1, strB);
        Try(i + 1, strB.append(str.charAt(i)));
        strB.deleteCharAt(strB.length() - 1);
    }

}
