package TN11;

import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class Email {
    static int n;
    static String s;
    static Set<String> ans;
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int t = input.nextInt();input.nextLine();
        while(t-->0){
            n = input.nextInt();input.nextLine();
            s = input.nextLine();
            ans = new TreeSet<>();
            dq(0, new StringBuffer(""));
            Iterator<String> it = ans.iterator();
            while(it.hasNext()){
                System.out.print(it.next()+" ");
            }
            System.out.println();
        }
    }
    static void dq(int idx, StringBuffer tmps){
        if(idx==n){
            if(tmps.length()>0) ans.add(new String(tmps));
            return;
        }
        dq(idx+1, tmps);
        dq(idx+1, tmps.append(s.charAt(idx)));
        tmps.deleteCharAt(tmps.length()-1);
    }
}