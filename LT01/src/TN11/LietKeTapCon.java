package TN11;

import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class LietKeTapCon {
    static int n;
    static String str;
    static Set<String> arrStr;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int test = in.nextInt();
        in.nextLine();

        while (test-- > 0) {
            n = in.nextInt();
            in.nextLine();
            str = in.nextLine();
            arrStr = new TreeSet<>();
            TapCon(0,new StringBuffer(""));
            Iterator<String> it = arrStr.iterator();
            while(it.hasNext()){
                System.out.print(it.next()+" ");
            }
            System.out.println();
        }
    }

    public static void TapCon(int i, StringBuffer strB) {
        if (i == n) {
            if (strB.length() > 0) {
                arrStr.add(new String(strB));
            }
            return;
        }
        TapCon(i + 1, strB);
        TapCon(i + 1, strB.append(str.charAt(i)));
        strB.deleteCharAt(strB.length() - 1);
    }

}
