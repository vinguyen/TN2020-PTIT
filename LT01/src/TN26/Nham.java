package TN26;

import java.util.*;

public class Nham {
    static int n, k, s, ans, tmp;
    static int[] c = new int[30];

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        while(true){
            n = input.nextInt();
            k = input.nextInt();
            s = input.nextInt();
            if(n==0 && k==0 && s==0){
                break;
            }
            ans = 0;
            tmp = 0;
            dq(1, 1);
            System.out.println(ans);
        }
    }
    static void dq(int val, int idx){
        if(val>n || idx>k){
            if(tmp==s && idx>k) ans++;
            return;
        }
        dq(val+1, idx);
        if(tmp+val<=s){
            tmp += val;
            dq(val+1, idx+1);
            tmp -= val;
        }
    }
}