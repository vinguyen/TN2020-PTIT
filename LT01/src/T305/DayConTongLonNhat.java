package T305;

import java.util.Scanner;

public class DayConTongLonNhat {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int n = in.nextInt();
            int[] A = new int[n + 1];
            for (int i = 1; i <= n; i++) {
                A[i] = in.nextInt();
            }
            int S=A[1];
            int E=A[1];
            int s = 1, e = 1;
            int s1 = 1;
            for (int i = 2; i <= n; i++) {
                if (E > 0) {
                    E = E + A[i];
                }
                else {
                    E = A[i];
                    s1 = i;
                }
                if (E > S) {
                    S = E;
                    e = i;
                    s = s1;
                }
            }
            System.out.println(S);
        }
    }
}
