package TN03;

import java.util.Arrays;
import java.util.Scanner;

public class TCS1 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String str = in.nextLine();
            Try(str);
        }

    }

    public static void Try(String str) {
        int sum = 0;
        int k = 0;
        char[] kq = new char[10001];
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') {
                kq[k++] = str.charAt(i);
            } else sum += (str.charAt(i) - 48);
        }
        Arrays.sort(kq,0,k);
        for (int i = 0; i < k; i++) {
            System.out.print(kq[i]);
        }
        System.out.println(sum);
    }

}
