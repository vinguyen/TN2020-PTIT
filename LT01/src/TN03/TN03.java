package TN03;

import java.util.Arrays;
import java.util.Scanner;

public class TN03 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String str = in.nextLine();
            char[] arrStr = new char[10001];
            int k=0;
            int sum=0;
            for (int i = 0; i < str.length(); i++) {
                if (str.charAt(i) >= 'A' && str.charAt(i) <= 'Z') {
                    arrStr[k++] = str.charAt(i);
                }
                else sum += (str.charAt(i)-48);
            }
            Arrays.sort(arrStr,0,k);
            for (int i = 0; i < k; i++) {
                System.out.print(arrStr[i]);
            }
            System.out.println(sum);
        }
    }

}
