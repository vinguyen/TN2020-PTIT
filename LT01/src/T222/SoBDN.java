package T222;

import java.util.Scanner;

public class SoBDN {

    public static long n;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();

        while (test-- > 0) {
            n = in.nextLong();
            String str = "";
            int dem = 0;
            while (true) {
                str = chuyenNhiPhan(str);
                if (Long.parseLong(str) > n) {
                    break;
                }
                dem++;
            }
            System.out.println(dem);
        }
    }

    public static String chuyenNhiPhan(String str) {
        int temp = 1;
        String kq = "";
        for (int i = str.length() - 1; i >= 0; i--) {
            int k = str.charAt(i)-'0'+temp;
            temp = 0;
            if (k == 2) {
                temp=1;
                k = 0;
            }
            kq = (char) ('0' + k) + kq;
        }
        if (temp == 1) {
            kq = '1' + kq;
        }
        return kq;
    }

}
