package T222;

import java.util.*;

public class SoBDN1 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String str = in.nextLine();
            String[] arrStr = str.split("");
            String res = "";
            for (int i = 0; i < arrStr.length; i++) {
                if (arrStr[i].equals("0") || arrStr[i].equals("1")) {
                    res += arrStr[i];
                }
                else {
                    for (int j = i; j < arrStr.length; j++) {
                        res += "1";
                    }
                    break;
                }
            }
            System.out.println(Integer.parseInt(res,2));
        }
    }

}
