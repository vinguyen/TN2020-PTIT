package TN24;

import java.util.*;

public class Nham {
    static int n, tmp;
    static int[] c = new int[30];

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int t = input.nextInt();input.nextLine();
        while(t-->0){
            n = input.nextInt();
            tmp = 0;
            dq(n);
            System.out.println();
        }
    }
    static void dq(int idx){
        if(idx<1) return;
        for(int i = (n-tmp)/idx; i>=0; i--){
            c[idx] = i;
            tmp += i*idx;
            if(tmp==n){
                System.out.print("(");
                for(int j = n; j >= idx; j--){
                    for(int k = 1; k <= c[j]; k++){
                        System.out.print(j);
                        if(k<c[j] || j > idx) System.out.print(" ");
                    }
                }
                System.out.print(") ");
            }else{
                dq(idx-1);
            }
            tmp -= i*idx;
            c[idx] = 0;
        }
    }
}