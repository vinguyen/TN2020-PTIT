package T219;

import java.util.Scanner;


public class main {
    public static Scanner sc;
    public static long[] a = new long[100009];
    public static int[] f = new int[100009];
    public static long ans;

    public static void ktra(int t, int p) {
        if (t > p) return;
        int vt = t;
        for (int i = t; i <= p; i++) {
            if (a[i] < a[vt]) {
                vt = i;
            }
        }
        ans = Math.max(ans, 1l * a[vt] * (p - t + 1l));
        ktra(t, vt - 1);
        ktra(vt + 1, p);
    }

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int test = sc.nextInt();
        while (test-- > 0) {
            int n = sc.nextInt();
            for (int i = 1; i <= n; i++) {
                a[i] = sc.nextLong();
            }
            ans = 0;
            ktra(1, n);
            System.out.println(ans);
        }
    }
}