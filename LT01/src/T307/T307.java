package T307;

import java.util.Scanner;

// Con ech
public class T307 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long[] F = new long[51];
        F[1] = 1;
        F[2] = 2;
        F[3] = 4;
        for (int i = 4; i <= 50; i++) {
            F[i] = F[i - 1] + F[i - 2] + F[i - 3];
        }
        int test = in.nextInt();
        while (test-- > 0) {
            int n = in.nextInt();
            System.out.println(F[n]);
        }
    }

}
