package TN09;

import java.util.Arrays;
import java.util.Scanner;

public class TN09__ {

    public static String[] arrStr;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            int n = in.nextInt();
            in.nextLine();
            String str = in.nextLine();
            Try(str, n);
        }
    }

    public static void Try(String str, int n) {
        int j = n - 2;
        arrStr = str.split(" ");
        while (j > -1 && Integer.parseInt(arrStr[j]) > Integer.parseInt(arrStr[j + 1])) {
            j--;
        }
        if (j > -1) {
            int k = n - 1;
            while (Integer.parseInt(arrStr[j]) > Integer.parseInt(arrStr[k])) {
                k--;
            }
            swap(j, k);
            int r = j + 1;
            int s = n - 1;

            while (r < s) {
                swap(r, s);
                r++;
                s--;
            }
        }
        else {
            Arrays.sort(arrStr);
        }
        StringBuilder kq = new StringBuilder();
        for (String s : arrStr) {
            kq.append(s);
            kq.append(" ");
        }
        System.out.println(kq.toString().trim());
    }

    public static void swap(int i, int j) {
        String temp = arrStr[i];
        arrStr[i] = arrStr[j];
        arrStr[j] = temp;
    }

}
