package T323;

import java.util.*;

// Duong di vo huong DFS
public class T323s {

    static boolean[] check;
    static ArrayList<ArrayList<Integer>> lists;
    static int[] before;

    public static void traceDFS(int s) {
        Stack<Integer> stack = new Stack<>();
        stack.push(s);
        check[s] = false;
        while (!stack.isEmpty()) {
            int u = stack.pop();
            for (int i = 0; i < lists.get(u).size(); i++) {
                int v = lists.get(u).get(i);
                if (check[v]) {
                    check[v] = false;
                    stack.push(u);
                    stack.push(v);
                    before[v] = u;
                    break;
                }
            }
        }

    }

    public static void trackBFS(int s) {
        Queue<Integer> queue = new LinkedList<>();
        queue.add(s);
        check[s] = false;
        while (!queue.isEmpty()) {
            int u = queue.poll();
            for (int i = 0; i < lists.get(u).size(); i++) {
                int v = lists.get(u).get(i);
                if (check[v]) {
                    check[v] = false;
                    queue.add(v);
                    before[v] = u;
                }
            }
        }
    }

    public static void solve(int s, int t) {
//        traceDFS(s);
        trackBFS(s);
        if (before[t] == 0) {
            System.out.println(-1);
        }
        else {
            Stack<Integer> stack = new Stack<>();
            stack.push(t);
            int u = before[t];
            while (u != s) {
                stack.push(u);
                u = before[u];
            }
            stack.push(s);
            while (!stack.isEmpty()) {
                System.out.print(stack.pop()+" ");
            }
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int v = in.nextInt();
            int e = in.nextInt();
            int s = in.nextInt();
            int t = in.nextInt();
            lists = new ArrayList<>();
            for (int i = 0; i <= v + 5; i++) {
                lists.add(new ArrayList<>());
            }
            check = new boolean[v + 5];
            before = new int[v + 5];
            for (int i = 1; i <= e; i++) {
                int a = in.nextInt();
                int b = in.nextInt();
                lists.get(a).add(b);
                lists.get(b).add(a);
            }
            Arrays.fill(check, true);
            solve(s, t);
            System.out.println();
        }
    }

}
