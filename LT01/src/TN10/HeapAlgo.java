package TN10;


import java.util.ArrayList;
import java.util.List;

class HeapAlgo {

    public List<String> arrStr = new ArrayList<>();

    void printArr(int a[], int n)
    {
        String str = "";
        for (int i = 0; i < n; i++) {
            str+=a[i];
        }
        arrStr.add(str);
    }
 
    // Generating permutation using Heap Algorithm
    void heapPermutation(int a[], int size, int n)
    {
        // if size becomes 1 then prints the obtained
        // permutation
        if (size == 1)
            printArr(a, n);
 
        for (int i = 0; i < size; i++) {
            heapPermutation(a, size - 1, n);
 
            // if size is odd, swap 0th i.e (first) and
            // (size-1)th i.e (last) element
            if (size % 2 == 1) {
                int temp = a[0];
                a[0] = a[size - 1];
                a[size - 1] = temp;
            }
 
            // If size is even, swap ith 
            // and (size-1)th i.e last element
            else {
                int temp = a[i];
                a[i] = a[size - 1];
                a[size - 1] = temp;
            }
        }
    }
 
    // Driver code
    public static void main(String args[])
    {
        HeapAlgo obj = new HeapAlgo();
        int a[] = { 1, 2, 3 };
        obj.heapPermutation(a, a.length, a.length);
        for (int i = obj.arrStr.size()-1; i>=0; i--) {
            System.out.println(obj.arrStr.get(i));
        }
    }
}
 
// This code has been contributed by Amit Khandelwal.