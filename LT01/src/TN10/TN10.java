package TN10;

import java.util.*;

public class TN10 {
    static int[] nums;
    static List<String> result;
    static int n;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();

        while (test-- > 0) {
            n = in.nextInt();
            nums = new int[n];
            for (int i = 0; i < n; i++) {
                nums[i] = i + 1;
            }
            result = new ArrayList<>();
            hoanViNguoc();
        }
    }

    public static void hoanViNguoc() {
        StringBuilder str = new StringBuilder();
        for (int num : nums) {
            str.append(num);
        }
        result.add(str.toString());
        nextPermutation(0);
        for (int i = result.size() - 1; i >= 1; i--) {
            System.out.print(result.get(i)+" ");
        }
        System.out.println(result.get(0));
    }

    public static void nextPermutation(int start) {
        int j = n - 2;
        while (j > -1 && nums[j] > nums[j + 1]) j--;
        if (j > -1) {
            int k = n - 1;
            while (nums[j]>nums[k]) k--;
            swap(j, k);
            int r = j + 1;
            int s = n - 1;
            while (r < s) {
                swap(r, s);
                r++;
                s--;
            }
            StringBuilder str = new StringBuilder();
            for (int num : nums) {
                str.append(num);
            }
            result.add(str.toString());
            nextPermutation(start + 1);
        }
    }

    public static void swap(int a, int b) {
        int temp = nums[a];
        nums[a] = nums[b];
        nums[b] = temp;
    }

}
