package TN10;

import java.util.Scanner;

public class TN10_ {

    static  int n;
    static int[] X = new int[20];
    static int[] check = new int[20];

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            n = in.nextInt();
            TryReverse(1);
            System.out.println();
            TryNext(1);
            System.out.println();
        }
    }

    public static void TryReverse(int i) {
        for (int j = n; j > 0; j--) {
            if (check[j] == 0) {
                X[i] = j;
                check[j]=1;
                if (i == n) {
                    for (int k = 1; k <= n; k++) {
                        System.out.print(X[k]);
                    }
                    System.out.print(" ");
                } else TryReverse(i + 1);
                check[j] = 0;
            }
        }
    }

    public static void TryNext(int i) {
        for (int j = 1; j <= n; j++) {
            if (check[j] == 0) {
                X[i] = j;
                check[j] = 1;
                if (i == n) {
                    for (int k = 1; k <= n; k++) {
                        System.out.print(X[k]);
                    }
                    System.out.print(" ");
                }
                else TryNext(i+1);
                check[j] = 0;
            }
        }
    }

}
