package T306;

import java.util.Scanner;

public class T306_ {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int n = in.nextInt();
            int[] A = new int[n + 5];
            int[] F = new int[n + 5];
            for (int i = 1; i <= n; i++) {
                A[i] = in.nextInt();
            }
            int res = 1;
            for (int i = 1; i <= n; i++) {
                F[i] = 1;
                for (int j = 1; j < i; j++) {
                    if (A[i] >= A[j]) {
                        F[i] = Math.max(F[i], F[j] + 1);
                    }
                }
                res = Math.max(F[i], res);
            }
            System.out.println(n - res);
        }
    }

}
