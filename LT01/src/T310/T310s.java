package T310;

import java.util.Scanner;

// Xau doi xung dai nhat
public class T310s {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String str = in.nextLine();
            int n = str.length();
            boolean[][] F = new boolean[n + 5][n + 5];
            for (int i = 1; i <= n; i++) {
                F[i][i] = true;
                F[i + 1][i] = true;
            }
            int res = 1;
            for (int k = 1; k <= n - 1; k++) {
                for (int i = 1; i <= n - k; i++) {
                    int j = i + k;
                    F[i][j] = str.charAt(i - 1) == str.charAt(j - 1) && F[i + 1][j - 1];
                    if (F[i][j]) {
                        res = Math.max(res, k + 1);
                    }
                }
            }
            System.out.println(res);
        }
    }

}
