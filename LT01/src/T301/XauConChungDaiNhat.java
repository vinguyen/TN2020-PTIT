package T301;

import java.util.Scanner;

public class XauConChungDaiNhat {
    static String s1,s2;
    static int n,m;
    static int[][] C;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        s1 = in.nextLine();
        s2 = in.nextLine();
        m = s1.length();
        n = s2.length();
        C = new int[1000][1000];
        for (int j = 0; j <= m; j++) {
            C[0][j] = 0;
        }
        for (int i = 0; i <= n; i++) {
            C[i][0] = 0;
        }
        Try();
    }

    public static void Try() {
        for (int j = 1; j <= m; j++) {
            for (int i = 1; i <= n; i++) {
                if (s1.charAt(j-1) == s2.charAt(i-1)) {
                    C[j][i] = C[j - 1][i - 1] + 1;
                }
                else {
                    C[j][i] = Math.max(C[j - 1][i], C[j][i - 1]);
                }
            }
        }
        System.out.println(C[m][n]);
    }

}
