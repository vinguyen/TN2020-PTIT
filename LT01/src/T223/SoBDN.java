package T223;

import java.util.Scanner;

public class SoBDN {

    public static int n;

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            n = in.nextInt();
            String str = "";
            while (true) {
                str = chuyenNhiPhan(str);
                if (Long.parseLong(str) % n == 0) {
                    System.out.println(str);
                    break;
                }
            }
        }
    }

    public static String chuyenNhiPhan(String str) {
        int temp = 1;
        String kq = "";
        for (int i = str.length() - 1; i >= 0; i--) {
            int k = str.charAt(i)-'0'+temp;
            temp = 0;
            if (k == 2) {
                temp=1;
                k = 0;
            }
            kq = (char) ('0' + k) + kq;
        }
        if (temp == 1) {
            kq = '1' + kq;
        }
        return kq;
    }

}
