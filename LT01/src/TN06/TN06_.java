package TN06;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TN06_ {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        List<String> list = new ArrayList<>();
        while (test-- > 0) {
            String str = in.nextLine();

            while (str.contains("  ")) {
                str = str.replaceAll("  ", " ");
            }

            str = str.trim();

            String[] arrStr = str.split(" ");

            StringBuilder kq = new StringBuilder();

            kq.append(arrStr[arrStr.length - 1].toLowerCase());
            for (int i = 0; i < arrStr.length - 1; i++) {
                kq.append(arrStr[i].substring(0,1).toLowerCase());
            }

            list.add(kq.toString());

            int count = 0;

            for (String s : list) {
                if (kq.toString().equals(s)) {
                    count++;
                }
            }

            System.out.println(kq.toString()+(count>1?count:"")+"@ptit.edu.vn");

        }
    }

}
