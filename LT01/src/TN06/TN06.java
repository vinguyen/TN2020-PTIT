package TN06;

import java.util.*;

public class TN06 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        List<String> list = new ArrayList<>();
        while (test-- > 0) {
            String str = in.nextLine();
            str = str.trim();

            while (str.contains("  ")) {
                str = str.replace("  ", " ");
            }
            String[] arrStr = str.split(" ");
            StringBuilder kq = new StringBuilder();

            kq.append(arrStr[arrStr.length - 1].toLowerCase());
            for (int i = 0; i < arrStr.length - 1; i++) {
                kq.append(arrStr[i].substring(0, 1).toLowerCase());
            }

            list.add(kq.toString());

            int count = 0;
            for (String s : list) {
                if (s.contains(kq.toString())) {
                    count++;
                }
            }
            System.out.println(kq.toString() + ((count > 1) ? count : "") + "@ptit.edu.vn");
        }
    }

}
