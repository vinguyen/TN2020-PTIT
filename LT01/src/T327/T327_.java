package T327;

import java.util.*;

public class T327_ {

    static ArrayList<ArrayList<Integer>> lists;
    static boolean[] check;
    static int[] before;
    static boolean res;

    public static void BFS(int u) {
        Queue<Integer> queue = new LinkedList<>();
        queue.add(u);
        check[u] = false;
        while (!queue.isEmpty()) {
            int v = queue.poll();
            for (int i = 0; i < lists.get(v).size(); i++) {
                int t = lists.get(v).get(i);
                if (check[t]) {
                    queue.add(t);
                    before[t] = v;
                    check[t] = false;
                } else if (t != before[v]) {
                    res = true;
                    break;
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int v = in.nextInt();
            int e = in.nextInt();
            lists = new ArrayList<>();
            for (int i = 0; i <= v + 5; i++) {
                lists.add(new ArrayList<>());
            }
            for (int i = 1; i <= e; i++) {
                int a = in.nextInt();
                int b = in.nextInt();
                lists.get(a).add(b);
                lists.get(b).add(a);
            }
            check = new boolean[v + 5];
            before = new int[v + 5];
            Arrays.fill(check, true);
            res = false;
            for (int i = 1; i <= v; i++) {
                if (check[i]) {
                    BFS(i);
                    if (res) {
                        break;
                    }
                }
            }
            if (res) {
                System.out.println("YES");
            } else System.out.println("NO");
        }
    }

}
