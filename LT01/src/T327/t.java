package T327;//  package tt;

import java.util.*;

public class t {
	static int t,  m;
	static ArrayList<ArrayList<Integer>> x;
	static int[] d = new int[1005];
	static int[] cha = new int[1005];
	static Queue<Integer> q = new LinkedList<Integer>();
	public static void bfs(int o) {
		q.clear();
		q.add(o);
		while(!q.isEmpty()) {
			int u = q.poll();
			for(int i: x.get(u))
				if(d[i]==0){
					d[i] = 1;
					cha[i] = u;
					q.add(i);
				} else if(i!=cha[u]) {
					m = 1;
					return;
				}
		}
	}
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);		
		t = in.nextInt(); //in.nextLine();
		while(t-->0){
			int n = in.nextInt();
			int e = in.nextInt();
			x = new ArrayList<ArrayList<Integer>>();
			for(int i = 0; i <= n; i++) {
//				ArrayList<Integer> tmp = new ArrayList();
				x.add(new ArrayList());
			}
			for(int i = 0; i < e; i++) {
				int u = in.nextInt();
				int v = in.nextInt();
				x.get(u).add(v);
				x.get(v).add(u);
			}
			for(int i=0;i<=n;i++) d[i] = cha[i] = 0;
			m = 0;
			for(int i=1;i<=n;i++)
				if(d[i]==0) {
					d[i] = 1;
					bfs(i);
					if(m==1) break;
				}
			if(m==1) System.out.println("YES");
			else System.out.println("NO");
		}
	}
}