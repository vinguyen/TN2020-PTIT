package T327;

import java.util.*;

// Kiem tra Chu Trinh
public class T327 {

    static ArrayList<ArrayList<Integer>> lists;
    static boolean[] check;
    static int[] dem;
    static boolean res;

    public static void BFS(int u) {
        Queue<Integer> queue = new LinkedList<>();
        queue.add(u);
        while (!queue.isEmpty()) {
            int v = queue.poll();
            for (int i = 0; i < lists.get(v).size(); i++) {
                int x = lists.get(v).get(i);
                if (!check[x]) {
                    check[x] = true;
                    dem[x] = v;
                    queue.add(x);
                } else if (x != dem[v]) {
                    res = true;
                    return;
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int v = in.nextInt();
            int e = in.nextInt();
            lists = new ArrayList<>();
            for (int i = 0; i <= v + 5; i++) {
                lists.add(new ArrayList<>());
            }
            for (int i = 1; i <= e; i++) {
                int a = in.nextInt();
                int b = in.nextInt();
                lists.get(a).add(b);
                lists.get(b).add(a);
            }
            check = new boolean[v + 5];
            dem = new int[v + 5];
            Arrays.fill(check, false);
            Arrays.fill(dem, 0);
            res = false;
            for (int i = 1; i <= v; i++) {
                if (!check[i]) {
                    check[i] = true;
                    BFS(i);
                    if (res) {
                        break;
                    }
                }
            }
            if (res) {
                System.out.println("YES");
            } else System.out.println("NO");
        }
    }

}
