package TN18;

import java.util.*;

public class Nham {
    static int n, x, tmp;
    static int[] a = new int[30];
    static int[] c = new int[30];
    static boolean ok;

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int t = input.nextInt();input.nextLine();
        while(t-->0){
            n = input.nextInt();
            x = input.nextInt();
            for(int i = 1; i <= n; i++){
                a[i] = input.nextInt();
            }
            ok = true;
            tmp = 0;
            dq(1);
            if(ok) System.out.println(-1);
            else System.out.println("]");
        }
    }
    static void dq(int idx){
        if(idx>n) return;
        for(int i = (x-tmp)/a[idx]; i>=0; i--){
            c[idx] = i;
            tmp += i*a[idx];
            if(tmp==x){
                if(ok){
                    System.out.print("[");
                    ok = false;
                }else{
                    System.out.print("][");
                }
                for(int j = 1; j <= idx; j++){
                    for(int k = 1; k <= c[j]; k++){
                        System.out.print(a[j]);
                        if(k<c[j] || j < idx) System.out.print(" ");
                    }
                }
//                System.out.print("] ");
            }else{
                dq(idx+1);
            }
            tmp -= i*a[idx];
            c[idx] = 0;
        }
    }
}