package TN01;

import java.util.Scanner;

public class Fibonacci {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int n = in.nextInt();

        long[] fn = new long[93];
        fn[1] = 1;
        fn[2] = 1;

        for (int i = 3; i <= 92; i++) {
            fn[i] = fn[i-1] + fn[i-2];
        }

        while (n-- > 0) {
            int fi = in.nextInt();
            System.out.println(fn[fi]);
        }

    }

}
