package TN01;

import java.util.Scanner;

public class TN01 {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int test = in.nextInt();
        long[] F = new long[93];
        F[1] = 1;
        F[2] = 1;
        for (int i = 3; i <= 92; i++) {
            F[i] = F[i - 1] + F[i - 2];
        }

        while (test-- > 0) {
            System.out.println(F[in.nextInt()]);
        }

    }

}
