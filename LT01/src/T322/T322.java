package T322;

import java.util.*;

// BFS Vo Huong
public class T322 {

    static ArrayList<ArrayList<Integer>> lists;
    static boolean[] check;
    static Queue<Integer> queue;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int v = in.nextInt();
            int e = in.nextInt();
            int u = in.nextInt();
            lists = new ArrayList<>();
            for (int i = 0; i <= v + 5; i++) {
                lists.add(new ArrayList<>());
            }
            for (int i = 1; i <= e; i++) {
                int a = in.nextInt();
                int b = in.nextInt();
                lists.get(a).add(b);
                lists.get(b).add(a);
            }
            check = new boolean[v + 5];
            queue = new LinkedList<>();
            Arrays.fill(check, true);
            BFS(u);
            System.out.println();
        }
    }

    public static void BFS(int u) {
        queue.add(u);
        check[u] = false;
        System.out.print(u + " ");
        while (!queue.isEmpty()) {
            int v = queue.poll();
            for (int i = 0; i < lists.get(v).size(); i++) {
                int t = lists.get(v).get(i);
                if (check[t]) {
                    queue.add(t);
                    System.out.print(t + " ");
                    check[t] = false;
                }
            }
        }
    }

}
