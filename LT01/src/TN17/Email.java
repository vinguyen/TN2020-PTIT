package TN17;

import java.util.*;

public class Email {
    static int n;
    static int k;
    static long ans;
    static Set<Long> set;
    static StringBuffer s;

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int t = input.nextInt();input.nextLine();
        while(t-->0){
            k = input.nextInt();input.nextLine();
            s = new StringBuffer(input.nextLine());
            n = s.length();
            ans = Long.parseLong(new String(s));
            set = new HashSet<>();
            set.add(ans);
            bfs();
            System.out.println(ans);
        }
    }
    static void bfs(){
        Queue<StringBuffer> q = new LinkedList<>();
        q.add(s);
        for(int step = 1; step <= k; step++){
            int sz = q.size();
            for(int l = 1; l <= sz; l++){
                StringBuffer tmp = q.poll();
                for(int i = 0; i < n; i++){
                    for(int j = 0; j < n; j++){
                        if(i==0 && s.charAt(j)=='0') continue;
                        char c = tmp.charAt(i);
                        tmp.setCharAt(i, tmp.charAt(j));
                        tmp.setCharAt(j, c);
                        long val = Long.parseLong(new String(tmp));
                        if(!set.contains(val)){
                            ans = Math.max(ans, val);
                            set.add(val);
                            q.add(new StringBuffer(tmp));
                        }
                        c = tmp.charAt(i);
                        tmp.setCharAt(i, tmp.charAt(j));
                        tmp.setCharAt(j, c);
                    }
                }
            }
        }
    }
}