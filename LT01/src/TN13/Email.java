package TN13;

import java.util.*;

public class Email {
    static int n;
    static long base = 1000000;
    static ArrayList<Long> a;
    static int[] t = new int[100005];
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int t = input.nextInt();
        while(t-->0){
            n = input.nextInt();
            a = new ArrayList<>();
            for(int i = 1; i <= n; i++){
                int bg = input.nextInt();
                int ed = input.nextInt();
                a.add(1l*ed*base+bg);
            }
            Collections.sort(a);
            long ans = 0, ed_time = 0;
            for(long x: a){
                long bg = x%base;
                if(bg>=ed_time){
                    ans++;
                    ed_time = (long)(x/base);
                }
            }
            System.out.println(ans);
        }
    }
}