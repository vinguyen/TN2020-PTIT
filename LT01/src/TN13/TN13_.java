package TN13;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class TN13_ {

    static int n;
    static long mod = 1000000;
    static ArrayList<Long> arr;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            n = in.nextInt();
            arr = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                int start = in.nextInt();
                int end = in.nextInt();
                arr.add(end * mod + start);
            }
            Collections.sort(arr);
            long kq = 0, endTime = 0;
            for (long l : arr) {
                long start = l%mod;
                if (start >= endTime) {
                    kq++;
                    endTime = l / mod;
                }
            }
            System.out.println(kq);
        }
    }

}
