package T319;

import java.util.ArrayList;
import java.util.Scanner;

public class T319__ {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        in.nextLine();
        ArrayList<ArrayList<Integer>> lists = new ArrayList<>();
        for (int i = 1; i <= n + 5; i++) {
            lists.add(new ArrayList<>());
        }
        for (int i = 1; i <= n; i++) {
            String str = in.nextLine();
            String[] arrStr = str.split(" ");
            for (String s : arrStr) {
                lists.get(i).add(Integer.parseInt(s));
            }
        }
        for (int i = 1; i <= n; i++) {
            for (int j = 0; j < lists.get(i).size(); j++) {
                if (lists.get(i).get(j) > i) {
                    System.out.println(i + " " + lists.get(i).get(j));
                }
            }
        }
    }

}
