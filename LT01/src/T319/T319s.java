package T319;

import java.util.ArrayList;
import java.util.Scanner;

// DS Ke sang canh
public class T319s {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        ArrayList<ArrayList<Integer>> lists = new ArrayList<>();
        for (int i = 0; i <= n + 5; i++) {
            lists.add(new ArrayList<>());
        }
        in.nextLine();
        for (int i = 1; i <= n; i++) {
            String str = in.nextLine();
            String[] arrStr = str.split(" ");
            for (String s : arrStr) {
                lists.get(i).add(Integer.parseInt(s));
            }
        }
        for (int i = 1; i <= n; i++) {
            for (int j = 0; j < lists.get(i).size(); j++) {
                if (lists.get(i).get(j) > i) {
                    System.out.print(i + " ");
                    System.out.println(lists.get(i).get(j));
                }
            }
        }
    }

}
