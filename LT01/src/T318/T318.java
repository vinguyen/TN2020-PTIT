package T318;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class T318 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int v = in.nextInt();
            int e = in.nextInt();
            List<Integer>[] lists = (List<Integer>[]) new List[v+5];
            for (int i = 0; i < lists.length; i++) {
                lists[i] = new ArrayList<>();
            }
            for (int i = 0; i < e; i++) {
                int a = in.nextInt();
                int b = in.nextInt();
                lists[a].add(b);
                lists[b].add(a);
            }
            for (int i = 1; i <= v; i++) {
                System.out.print(i + ": ");
                for (int j = 0; j < lists[i].size(); j++) {
                    System.out.print(lists[i].get(j) + " ");
                }
                System.out.println();
            }
        }
    }
}
