package TN04;

import java.util.Scanner;

public class TN04 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String str = in.nextLine();

            String[] arrStr = str.split("");

            int n = str.length();

            String check = "YES";

            for (int i = 0; i <= n / 2; i++) {
                if (Integer.parseInt(arrStr[i]) % 2 == 1 || Integer.parseInt(arrStr[i]) != Integer.parseInt(arrStr[n - i -1])) {
                    check = "NO";
                    break;
                }
            }
            System.out.println(check);
        }
    }

}
