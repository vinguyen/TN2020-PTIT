package T220;

import java.util.Scanner;

public class T220 {
	public static Scanner sc;

    public static int[] x = new int[12];
    public static int[] a = new int[12];
    public static int[] ans = new int[12];
    public static int n;
    public static String s;

    public static void backTrack(int k) {
        for (int i = 1; i < 10; i++){
            if (x[i] == 0)
            {
                a[k] = i;
                x[i] = 1;
                if ( k == n ) {
                    int ok = 0;
                    for (int j = 1; j <= n; j++) {
                        if (a[j] < ans[j]) {
                            ok = 1;
                            break;
                        } else if (a[j] > ans[j] ) {
                            break;
                        }
                    }
                    int ok1 = 1;
                    for (int j = 2; j <= n; j++) {
                        if (s.charAt(j-2) == 'I') {
                            if (a[j] < a[j-1]) ok1 = 0;
                        } else {
                            if (a[j] > a[j-1]) ok1 = 0;
                        }
                    }
                    if ((ok == 1 || ans[1] == 0 && ok1 == 1) ) {
                        for (int j = 1; j <= n; j++) {
                            ans[j] = a[j];
                        }
                    }
                } else {
                    backTrack(k+1);
                }
                x[i] = 0;
            }
        }
    }

    public static void xuli() {
        s = sc.nextLine();
        n = s.length();
        n++;
        for (int i = 0; i < 12; i++) {
            ans[i] = x[i] = a[i] = 0;
        }

        backTrack(1);

        for (int i = 1; i <= n; i++) {
            System.out.print(ans[i]);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int tc = Integer.parseInt(sc.nextLine());
        while (tc-- > 0) {
            xuli();
        }
    }
}