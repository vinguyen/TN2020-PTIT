package T320;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DoThiCoHuong {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int v = in.nextInt();
            int e = in.nextInt();
            List<Integer>[] E = (List<Integer>[]) new List[1005];
            for (int i = 0; i < E.length; i++) {
                E[i] = new ArrayList<Integer>();
            }
            for (int i = 0; i < e; i++) {
                int a = in.nextInt();
                int b = in.nextInt();
                E[a].add(b);
            }

            for (int i = 1; i <= v; i++) {
                System.out.print(i + ": ");
                for (int j = 0; j < E[i].size(); j++) {
                    System.out.print(E[i].get(j) + " ");
                }
                System.out.println();
            }
        }
    }

}
