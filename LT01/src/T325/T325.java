package T325;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

//Dem so thanh phan lien thong
public class T325 {

    static ArrayList<ArrayList<Integer>> lists;
    static boolean[] check;

    public static void strongConnective(int u) {
        check[u] = true;
        for (int i = 0; i < lists.get(u).size(); i++) {
            int v = lists.get(u).get(i);
            if (!check[v]) {
                strongConnective(v);
            }
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int v = in.nextInt();
            int e = in.nextInt();
            lists = new ArrayList<>();
            for (int i = 0; i <= v + 5; i++) {
                lists.add(new ArrayList<>());
            }
            check = new boolean[v + 5];
            for (int i = 1; i <= e; i++) {
                int a = in.nextInt();
                int b = in.nextInt();
                lists.get(a).add(b);
                lists.get(b).add(a);
            }
            Arrays.fill(check, false);
            int res =0;
            for (int i = 1; i <= v; i++) {
                if (!check[i]) {
                    res++;
                    strongConnective(i);
                }
            }
            System.out.println(res);
        }
    }

}
