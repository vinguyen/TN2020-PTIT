package TN15;

import java.util.Scanner;

public class TN15 {

    static int[][] arr = new int[20][20];
    static int n;
    static boolean has_road = true;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            n = in.nextInt();
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= n; j++) {
                    arr[i][j] = in.nextInt();
                }
            }
            if (arr[n][n]==0) {
                System.out.println("-1");
                continue;
            }
            has_road = false;
            timDuong(1, 1, new StringBuffer(""));
            if (!has_road) {
                System.out.println("-1");
            }else System.out.println();
        }
    }

    public static void timDuong(int i, int j, StringBuffer strB) {
        if (i == n && j == n) {
            System.out.print(strB + " ");
            has_road = true;
            return;
        }
        if (i < n && arr[i + 1][j] == 1) {
            timDuong(i + 1, j, strB.append("D"));
            strB.deleteCharAt(strB.length() - 1);
        }
        if (j < n && arr[i][j + 1] == 1) {
            timDuong(i, j + 1, strB.append("R"));
            strB.deleteCharAt(strB.length() - 1);
        }
    }

}
