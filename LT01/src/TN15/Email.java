package TN15;

import java.util.*;

public class Email {
    static int n;
    static int[][] a = new int[20][20];
    static boolean has_road;

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int t = input.nextInt();
        while(t-->0){
            n = input.nextInt();
            for(int i = 1; i <= n; i++){
                for(int j = 1; j <= n; j++){
                    a[i][j] = input.nextInt();
                }
            }
            if(a[1][1]==0){
                System.out.println(-1);
                continue;
            }
            has_road = false;
            dq(1, 1, new StringBuffer(""));
            if(!has_road){
                System.out.println(-1);
            }else{
                System.out.println();
            }
        }
    }

    static void dq(int r, int c, StringBuffer s){
        if(r==n&&c==n){
            System.out.print(s+" ");
            has_road = true;
            return;
        }
        if(r<n && a[r+1][c]==1){
            dq(r+1, c, s.append('D'));
            s.deleteCharAt(s.length()-1);
        }
        if(c<n && a[r][c+1]==1){
            dq(r, c+1, s.append('R'));
            s.deleteCharAt(s.length()-1);
        }
    }
}