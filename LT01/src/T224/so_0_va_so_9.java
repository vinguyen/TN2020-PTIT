package T224;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Truong-nd
 */
public class so_0_va_so_9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int test = input.nextInt();
        while(test>0){
            
            int n= input.nextInt();
            ArrayList<Long> lists =  new ArrayList<Long>();
            lists.add(Long.parseLong("9"));
            while(true){
                long res = lists.get(0);lists.remove(0);
                long tinh = res%n;
                if(tinh==0){
                    System.out.println(res);
                    break;
                }
                lists.add(Long.parseLong(res+"0"));
                lists.add(Long.parseLong(res+"9"));
            }
            
            test--;
        }
    }
}