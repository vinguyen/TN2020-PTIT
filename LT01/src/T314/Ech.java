package T314;//  package tt;

import java.util.Scanner;

public class Ech {
	static int n, v, t;
	static long[][] f = new long[1005][1005];
	static int[] vol = new int[1005];
	static int[] wei = new int[1005];
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		t = in.nextInt();
		while(t-->0) {
			n = in.nextInt();
			v = in.nextInt();
			for(int i = 1; i <= n; i++) vol[i] = in.nextInt();
			for(int i = 1; i <= n; i++) wei[i] = in.nextInt();
			for(int i = 1; i <= n; i++) for(int j = 1; j <= v; j++) f[i][j] = 0;
			long ans = 0;
			for(int i = 1; i <= n; i++) {
				f[i][vol[i]] = Math.max(wei[i], f[i-1][vol[i]]);
				for(int j = 1; j <= v; j++) {
					f[i][j] = Math.max(f[i-1][j], f[i][j]);
					if(f[i-1][j] != 0 && j+vol[i]<=v) {
						f[i][j+vol[i]] = Math.max(f[i][j+vol[i]], f[i-1][j]+wei[i]);
					}
					ans = Math.max(ans, f[i][j]);
				}
			}
//			for(int i = 1; i <= n; i++) {
//				for(int j = 1; j <= v; j++) {
//					System.out.print(f[i][j]+" ");
//				}
//				System.out.println();
//			}
			System.out.println(ans);
		}
	}
}