package T314;

import java.util.Scanner;

public class CaiTui {
    static int v;
    static int n;
    static int[] a;
    static int[] c;
    static long[][] maxV;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int test = in.nextInt();

        while (test-- > 0) {
            n = in.nextInt();
            v = in.nextInt();
            maxV = new long[n + 5][v + 5];
            a = new int[n + 5];
            c = new int[n + 5];
            for (int i = 1; i <= n; i++) {
                a[i] = in.nextInt();
            }
            for (int i = 1; i <= n; i++) {
                c[i] = in.nextInt();
            }
            Try();
        }
    }

    public static void Try() {
        for (int i = 1; i <= n; i++) {
            maxV[i][a[i]] = Math.max(c[i],maxV[i-1][a[i]]);
            for (int L = 1; L <= v; L++) {
                if (L < a[i]) {
                    maxV[i][L] = maxV[i - 1][L];
                }
                else {
                    maxV[i][L] = Math.max(maxV[i - 1][L], c[i] + maxV[i - 1][L - a[i]]);
                }
            }
        }
        System.out.println(maxV[n][v]);
    }
}
