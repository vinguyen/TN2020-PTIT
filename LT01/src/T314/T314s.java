package T314;

import java.util.Scanner;

// Cai Tui
public class T314s {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int n = in.nextInt();
            int v = in.nextInt();
            int[] a = new int[n + 5];
            int[] c = new int[n + 5];
            for (int i = 1; i <= n; i++) {
                a[i] = in.nextInt();
            }
            for (int i = 1; i <= n; i++) {
                c[i] = in.nextInt();
            }
            int[][] maxV = new int[n + 5][v + 5];
            for (int i = 1; i <= n; i++) {
                maxV[i][a[i]] = Math.max(c[i], maxV[i - 1][a[i]]);
                for (int j = 1; j <= v; j++) {
                    if (j >= a[i]) {
                        maxV[i][j] = Math.max(maxV[i - 1][j], maxV[i - 1][j - a[i]] + c[i]);
                    }
                    else {
                        maxV[i][j] = maxV[i - 1][j];
                    }
                }
            }
            System.out.println(maxV[n][v]);

        }
    }
}
