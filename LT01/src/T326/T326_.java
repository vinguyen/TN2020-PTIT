package T326;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class T326_ {

    static ArrayList<ArrayList<Integer>> lists;
    static boolean[] check;

    public static void DFS(int u) {
        check[u] = false;
        for (int i = 0; i < lists.get(u).size(); i++) {
            int v = lists.get(u).get(i);
            if (check[v]) {
                DFS(v);
            }
        }
    }


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int v = in.nextInt();
            int e = in.nextInt();
            lists = new ArrayList<>();
            for (int i = 0; i <= v + 5; i++) {
                lists.add(new ArrayList<>());
            }
            for (int i = 1; i <= e; i++) {
                int a = in.nextInt();
                int b = in.nextInt();
                lists.get(a).add(b);
                lists.get(b).add(a);
            }
            check = new boolean[v + 5];
            Arrays.fill(check, true);
            int res = 0;
            for (int i = 1; i <= v; i++) {
                if (check[i]) {
                    res++;
                    DFS(i);
                }
            }
            for (int i = 1; i <= v; i++) {
                Arrays.fill(check,true);
                check[i] = false;
                int count = 0;
                for (int j = 1; j <= v; j++) {
                    if (check[j]) {
                        count++;
                        DFS(j);
                    }
                }
                if (count > res) {
                    System.out.print(i+" ");
                }
            }
            System.out.println();
        }
    }

}
