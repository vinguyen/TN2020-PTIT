package T326;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

// Liet ke dinh tru
public class T326 {

    static ArrayList<ArrayList<Integer>> lists;
    static boolean[] check;

    public static void DFS(int u) {
        check[u] = true;
        for (int i = 0; i < lists.get(u).size(); i++) {
            int v = lists.get(u).get(i);
            if (!check[v]) {
                DFS(v);
            }
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int v = in.nextInt();
            int e = in.nextInt();
            lists = new ArrayList<>();
            for (int i = 0; i <= v + 5; i++) {
                lists.add(new ArrayList<>());
            }
            check = new boolean[v + 5];
            for (int i = 1; i <= e; i++) {
                int a = in.nextInt();
                int b = in.nextInt();
                lists.get(a).add(b);
                lists.get(b).add(a);
            }
            int res = 0;
            Arrays.fill(check, false);
            for (int i = 1; i <= v; i++) {
                if (!check[i]) {
                    res++;
                    DFS(i);
                }
            }
            for (int i = 1; i <= v; i++) {
                Arrays.fill(check, false);
                check[i] = true;
                int count = 0;
                for (int j = 1; j <= v; j++) {
                    if (!check[j]) {
                        count++;
                        DFS(j);
                    }
                }
                if (count > res) {
                    System.out.print(i+" ");
                }
            }
            System.out.println();
        }
    }

}
