package T303;

import java.util.Scanner;

// Day con chung
public class T303 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int x = in.nextInt();
            int y = in.nextInt();
            int z = in.nextInt();
            in.nextLine();
            String str = in.nextLine();
            String[] arrStr = str.split(" ");
            String X = arrStr[0];
            String Y = arrStr[1];
            String Z = arrStr[2];
            int[][][] C = new int[x + 5][y + 5][z + 5];
            for (int i = 1; i <= x; i++) {
                char temp = X.charAt(i-1);
                for (int j = 1; j <= y; j++) {
                    for (int k = 1; k <= z; k++) {
                        if (temp == Y.charAt(j-1) && temp == Z.charAt(k-1)) {
                            C[i][j][k] = C[i - 1][j - 1][k - 1] + 1;
                        }
                        else {
                            C[i][j][k] = Math.max(C[i - 1][j][k], Math.max(C[i][j - 1][k], C[i][j][k - 1]));
                        }
                    }
                }
            }
            System.out.println(C[x][y][z]);
        }
    }

}
