package T303;

import java.util.Scanner;

public class T303_ {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int x = in.nextInt();
            int y = in.nextInt();
            int z = in.nextInt();
            in.nextLine();
            String str = in.nextLine();
            String[] arrStr = str.split(" ");
            String X = arrStr[0];
            String Y = arrStr[1];
            String Z = arrStr[2];
            int[][][] F = new int[x + 5][y + 5][z + 5];
            for (int i = 1; i <= x; i++) {
                for (int j = 1; j <= y; j++) {
                    for (int k = 1; k <= z; k++) {
                        if (X.charAt(i-1) == Y.charAt(j-1) && Y.charAt(j-1) == Z.charAt(k-1)) {
                            F[i][j][k] = F[i-1][j-1][k-1]+1;
                        }
                        else {
                            F[i][j][k] = Math.max(F[i - 1][j][k], Math.max(F[i][j - 1][k], F[i][j][k - 1]));
                        }
                    }
                }
            }
            System.out.println(F[x][y][z]);
        }
    }

}
