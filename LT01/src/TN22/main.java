package TN22;

import java.util.Scanner;

public class main {

    public static Scanner sc;
    public static int[][] a = new int[10][10];
    public static int[] x = new int[35];
    public static int[] row = new int[35];
    public static int[] cheo1 = new int[35];
    public static int[] cheo2 = new int[35];
    public static int n;
    public static int ds;

    public static void ktra(int k) {
        if (k > n) {
            int tong = 0;
            for (int i = 1; i <= n; i++) {
                tong += a[i][x[i]];
            }
            if (tong > ds) {
                ds = tong;
            }
            return;
        }
        for (int i = 1; i <= n; i++) {
            if (row[i] == 0 && cheo1[i + k] == 0 && cheo2[i - k + n] == 0) {
                x[k] = i;
                row[i] = 1;
                cheo1[i + k] = 1;
                cheo2[i - k + n] = 1;
                ktra(k + 1);
                row[i] = 0;
                cheo1[i + k] = 0;
                cheo2[i - k + n] = 0;
            }
        }
    }

    public static void main(String arg[]) {
        sc = new Scanner(System.in);
        int test = sc.nextInt();
        while (test-- > 0) {
            n = 8;
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= n; j++) {
                    a[i][j] = sc.nextInt();
                }
            }
            for (int i = 0; i <= 2 * n; i++) {
                row[i] = 0;
                cheo1[i] = 0;
                cheo2[i] = 0;
            }
            ds = 0;
            ktra(1);
            System.out.println(ds);
        }
    }
}