package T201;

import java.util.Scanner;

public class DoiChoTrucTiep {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int n = in.nextInt();

        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = in.nextInt();
        }

        for (int i = 0; i < n-1; i++) {
            for (int j = i+1; j < n; j++) {
                if (arr[i] > arr[j]) {
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
            System.out.print("Buoc "+(i+1)+": ");
            for (int k = 0; k < n-1; k++) {
                System.out.print(arr[k]+" ");
            }
            System.out.print(arr[n-1]);
            System.out.println();
        }

    }

}
