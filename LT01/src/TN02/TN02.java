package TN02;

import java.util.Scanner;

public class TN02 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int test = in.nextInt();
        while (test-- > 0) {
            long n = in.nextLong();
            Try(n);
        }
    }

    public static void Try(long n) {
        for (long i = 2; i <= Math.sqrt(n); i++) {
            while (n % i == 0) {
                n/=i;
            }
        }
        if (n > 2) {
            System.out.println(n);
        }
        else System.out.println(2);
    }

}
