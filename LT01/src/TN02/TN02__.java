package TN02;

import java.util.Scanner;

public class TN02__ {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            long n = in.nextLong();
            for (long i = 2; i <= Math.sqrt(n); i++) {
                while (n % i == 0) {
                    n /= i;
                }
            }
            System.out.println(Math.max(n, 2));
        }
    }
}
