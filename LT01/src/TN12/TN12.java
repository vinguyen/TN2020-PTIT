package TN12;

import java.util.Scanner;

public class TN12 {

    static int n,k;
    static int[] arr;
    static boolean OK;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            n = in.nextInt();
            k = in.nextInt();
            arr = new int[k];
            OK = true;
            for (int i = 0; i < k; i++) {
                arr[i] = in.nextInt();
                if (arr[i] != n - k + i + 1) {
                    OK = false;
                }
            }

            System.out.println(Try());
        }
    }

    public static int Try() {
        if (OK) {
            return k;
        }
        else {
            int[] _arr = new int[k];
            if (k >= 0) System.arraycopy(arr, 0, _arr, 0, k);
            for (int i = k - 1; i >= 0; i--) {
                if (arr[i] != (n - k + i + 1)) {
                    arr[i]+=1;
                    for (int j = i + 1; j < arr.length; j++) {
                        arr[j]=arr[j-1]+1;
                    }
                    break;
                }
            }
            int dem = 0;
            for (int i = 0; i < k; i++) {
                for (int j = 0; j < k; j++) {
                    if (arr[i] == _arr[j]) {
                        dem++;
                        break;
                    }
                }
            }
            return k-dem;
        }

    }

}
