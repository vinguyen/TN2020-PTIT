package TN12;

import java.util.Scanner;

public class SinhToHop {
    static int n,k;
    static int[] X;
    static boolean OK;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            n = in.nextInt();
            k = in.nextInt();
            X = new int[k];
            OK = true;
            for (int i = 0; i < k; i++) {
                X[i] = i+1;
                System.out.print(X[i]);
            }
            System.out.print(" ");
            while (OK) {
                nextCombination();
            }
            System.out.println();
        }

    }

    public static void nextCombination() {
        int i = k-1;
        while (i>-1 && X[i]==n-k+i+1) i--;
        if (i > -1) {
            X[i] = X[i]+1;
            for (int j = i + 1; j < k; j++) {
                X[j] = X[i] + j - i;
            }
            for (int t = 0; t < k; t++) {
                System.out.print(X[t]);
            }
            System.out.print(" ");
        }
        else OK=false;
    }

}
