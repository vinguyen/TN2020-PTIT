package T226;

import java.util.Scanner;

/**
 *
 * @author KyThuat88
 */
public class BienDoiS_T {

    /**
     * @param args the command line arguments
     */
    public static int tinh(int s, int t){
        if(s>=t) return s-t;
        int dem=0;
        if(t%2==1){
            dem=1;
            t++;
        }
        return (dem+1+ tinh(s, t/2));
        
    }
    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        int test= sc.nextInt();
        while(test-->0){
            int s= sc.nextInt();
            int t=sc.nextInt();
            
            int dem=0;
            if(s>=t) dem=s-t;
            else{
                
                dem=tinh(s,t);
            }
            
            System.out.println(dem);
        }
    }
    
}