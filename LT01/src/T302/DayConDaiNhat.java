package T302;

import java.util.Arrays;
import java.util.Scanner;

public class DayConDaiNhat {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int n = in.nextInt();
            in.nextLine();
            String str = in.nextLine();
//            Try(str, n);
            TryStr(str,n);
        }
    }

    public static void Try(String str,int n) {

        String[] arrStr = new String[n];
        for (int i = 0; i < n; i++) {
            arrStr[i] = str.substring(i, n);
        }

        Arrays.sort(arrStr);

        String kq = "";
        for (int i = 0; i < n - 1; i++) {
            String x = strCommon(arrStr[i], arrStr[i + 1]);
            if (x.length() > kq.length()) {
                kq = x;
            }
        }

        System.out.println(kq.length());

    }

    public static String strCommon(String a, String b) {
        int n = Math.min(a.length(), b.length());
        for (int i = 0; i < n; i++) {
            if (a.charAt(i) != b.charAt(i)) {
                return a.substring(0, i);
            }
        }
        return a.substring(0, n);
    }

    public static void TryStr(String str, int n) {
        int min = 0;

        for (int i = 0; i < n-1; i++) {
            for (int j = i+1; j < n; j++) {
                if (str.charAt(i) == str.charAt(j)) {
                    min++;
                    break;
                }
            }
        }
        System.out.println(min);
    }


}
