package T302;

import java.util.Scanner;

public class T302_ {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int n = in.nextInt();
            in.nextLine();
            String str = in.nextLine();
            int[][] F =new int[n + 5][n + 5];
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= n; j++) {
                    if (str.charAt(i-1) == str.charAt(j-1) && i != j) {
                        F[i][j] = F[i-1][j-1]+1;
                    }
                    else {
                        F[i][j] = Math.max(F[i - 1][j], F[i][j - 1]);
                    }
                }
            }
            System.out.println(F[n][n]);
        }
    }

}
