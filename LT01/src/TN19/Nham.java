package TN19;

import java.util.*;

public class Nham {
    static long ans;
    static String s;
    static int t, n;
    static StringBuffer tmp;

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        t = input.nextInt(); input.nextLine();
        while(t-->0){
            s = input.nextLine();
            ans = -1;
            n = s.length();
            tmp = new StringBuffer("");
            dq(1);
            System.out.println(ans);
        }
    }
    static void dq(int idx){
        if(idx>n){
            if(tmp.length()>0){
                long val = Long.parseLong(new String(tmp));
                long cbb = (long)Math.round(Math.pow((double)val, 1.0/3.0));
//                if(val==125) System.out.println(cbb);
                if(cbb*cbb*cbb==val){
                    ans = Math.max(ans, val);
                }
            }
            return;
        }
        dq(idx+1);
        tmp.append(s.charAt(idx-1));
        dq(idx+1);
        tmp.deleteCharAt(tmp.length()-1);
    }
}