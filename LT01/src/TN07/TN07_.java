package TN07;

import java.util.Scanner;

public class TN07_ {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String str = in.nextLine();
        if (!Try(str).isEmpty()) {
            System.out.println(Try(str));
        } else {
            System.out.println("Empty String");
        }
    }

    public static String Try(String str) {
        for (int i = 0; i < str.length() - 1; i++) {
            if (str.charAt(i) == str.charAt(i + 1)) {
                return Try(str.replace(str.substring(i, i + 2), ""));
            }
        }
        return str;
    }
}
