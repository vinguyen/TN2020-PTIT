package T221;

import java.util.Scanner;

public class main {

    public static Scanner sc;
    public static int n;

    public static String ktra(String s) {
        int tam = 1;
        String ans = "";
        for (int i = s.length() - 1; i >= 0; i--) {
            int k = s.charAt(i) - '0' + tam;
            tam = 0;
            if (k == 2) {
                tam = 1;
                k = 0;
            }
            ans = (char) ('0' + k) + ans;
        }
        if (tam == 1) {
            ans = '1' + ans;
        }
        return ans;
    }

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int test = sc.nextInt();
        while (test-- > 0) {
            n = sc.nextInt();
            String ans = "";
            for (int i = 0; i < n; i++) {
                ans = ktra(ans);
                System.out.print(ans + " ");
            }
            System.out.println();
        }
    }
}