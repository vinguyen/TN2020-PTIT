package T221;

import java.util.Scanner;

public class SoNhiPhan {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            int n = in.nextInt();
            for (int i = 1; i <= n; i++) {
                System.out.print(Integer.toBinaryString(i)+" ");
            }
            System.out.println();
        }
    }

}
