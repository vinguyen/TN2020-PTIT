package T228;

import java.util.Scanner;

public class DiChuyenTrongMaTran {
    static int n, m;
    static int[][] A;
    static int[][] Ax;
    static int[] Br = new int[10000000];
    static int[] Bc = new int[10000000];
    static int s,e,tr,tc,tm,tpr,tpc;

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        while (test-- > 0) {
            n = in.nextInt();
            m = in.nextInt();
            A = new int[n + 5][m + 5];
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= m; j++) {
                    A[i][j] = in.nextInt();
                }
            }
            Ax = new int[n + 5][n + 5];
            System.out.println(Try());;
        }
    }

    public static int Try() {
        s = 0;
        e = 0;
        Br[0] = 1;
        Bc[0] = 1;
        Ax[1][1] = 1;
        while (s <= e) {
            tr = Br[s];
            tc = Bc[s];
            tm = Ax[tr][tc];
            int step = A[tr][tc];
            s++;
            if (tr == n && tc == m) {
                return tm-1;
            }
            tpr = tr;
            tpc = tc + step;
            check();
            tpr = tr + step;
            tpc = tc;
            check();
        }
        return -1;
    }

    public static void check() {
        if (tpr >= 1 && tpr <= n && tpc >= 1 && tpc <= m && Ax[tpr][tpc] == 0) {
            e++;
            Br[e] = tpr;
            Bc[e] = tpc;
            Ax[tpr][tpc] = tm +1;
        }
    }

}
