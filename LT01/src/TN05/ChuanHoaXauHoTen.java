package TN05;

import java.util.Scanner;

public class ChuanHoaXauHoTen {

    public static String chuanHoa(String str) {

        str = str.trim();

        while (str.contains("  ")) {
            str = str.replace("  "," ");
        }
        String[] arrStr = str.split(" ");

        StringBuilder kq = new StringBuilder();
        for (String s : arrStr) {
            kq.append(s.substring(0, 1).toUpperCase());
            kq.append(s.substring(1).toLowerCase());
            kq.append(" ");
        }

        return kq.toString().trim();
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int n = in.nextInt();
        in.nextLine();

        while (n-- > 0) {
            String str = in.nextLine();
            System.out.println(chuanHoa(str));
        }
    }

}
