package TN05;

import java.util.Scanner;

public class TN05 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String str = in.nextLine();
            str = str.trim();

            while (str.contains("  ")) {
                str = str.replace("  ", " ");
            }

            String[] arrStr = str.split(" ");
            StringBuilder kq = new StringBuilder();
            for (String s : arrStr) {
                kq.append(s.substring(0, 1).toUpperCase());
                kq.append(s.substring(1).toLowerCase());
                kq.append(" ");
            }
            System.out.println(kq.toString().trim());
        }

    }

}
