package TN05;

import java.util.Scanner;

public class TN05_ {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        in.nextLine();
        while (test-- > 0) {
            String str = in.nextLine();
            while (str.contains("  ")) {
                str = str.replaceAll(" {2}", " ");
            }
            str = str.trim();

            String[] arrStr = str.split(" ");

            StringBuilder kq = new StringBuilder();
            for (String s : arrStr) {
                kq.append(s.substring(0, 1).toUpperCase());
                kq.append(s.substring(1).toLowerCase()).append(" ");
            }
            System.out.println(kq.toString().trim());
        }
    }

}
