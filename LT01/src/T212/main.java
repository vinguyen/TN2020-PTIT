package T212;

import java.util.Scanner;
import java.util.Stack;

public class main {

    public static Scanner sc;
    public static String s;

    public static void ktra() {
        Stack<Character> st = new Stack<Character>();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                st.push('(');
            } else {
                if (s.charAt(i) == ')') {
                    char x = st.pop();
                    if (x == '(') {
                        System.out.println("Yes");
                        return;
                    }
                    while (x != '(') {
                        x = st.pop();
                    }
                } else {
                    if ('a' <= s.charAt(i) && s.charAt(i) <= 'z') {
                        continue;
                    }
                    st.push(s.charAt(i));
                }
            }
        }
        System.out.println("No");
    }

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        int test = sc.nextInt();
        s = sc.nextLine();
        while (test-- > 0) {
            s = sc.nextLine();
            ktra();
        }
    }
}